package com.j2core.week01.helloworld;

/**
 * Created by vip on 17.05.2016.
 * HelloWorld class uses for print text "Hello World!" in to console
 */
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}